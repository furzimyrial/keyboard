## Pure HTML/JS/CSS keyboard plugin

### How to download and install:

* Clone from the gitlab:
`git clone https://gitlab.com/furzimyrial/keyboard.git`

* Copy the KEYBOARD folder to "YOUR_PROJECT/PLUGINS/"

* Include 'keyboard.js' file at your html head element:
`<script src="PLUGINS/KEYBOARD/KEYBOARD.js"></script>`

* Add keyboard element wherever you want:
`<keyboard></keyboard>`

